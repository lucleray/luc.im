import { InlineCode } from '../components/inline'

export default [
  {
    id: 'object-detection',
    date: 'March 2019',
    title: 'Object Detection',
    url: 'https://object-detection.now.sh',
    about:
      'An API built with Tensorflow and Lambda functions to predict objects on images'
  },
  {
    id: 'tensorflow-lambda',
    date: 'March 2019',
    title: <InlineCode>tensorflow-lambda</InlineCode>,
    url: 'https://github.com/lucleray/tensorflow-lambda',
    about: 'A package to run tensorflow on lambda functions'
  },
  {
    id: 'next-size',
    date: 'November 2018',
    title: <InlineCode>next-size</InlineCode>,
    url: 'https://github.com/lucleray/next-size',
    about: 'Next.js plugin to print browser assets sizes'
  },
  {
    id: 'emoji-machine',
    date: 'October 2018',
    title: '🎰Emoji Machine',
    url: 'http://emoji-machine.now.sh/',
    about: 'emoji suggestions for your thoughts'
  },
  {
    id: 'babel-macro-styled-components',
    date: 'October 2018',
    title: <InlineCode>styled-components/macro</InlineCode>,
    url: 'https://www.styled-components.com/docs/tooling#babel-macro',
    about: 'a babel macro for styled-components'
  },
  {
    id: 'sql',
    date: 'August 2018',
    title: <InlineCode>sql</InlineCode>,
    url: 'https://github.com/sequencework/sql',
    about: 'javascript tag to format SQL template literals'
  },
  {
    id: 'terminal-markdown',
    date: 'June 2018',
    title: <InlineCode>terminal-markdown</InlineCode>,
    url: 'https://github.com/lucleray/terminal-markdown',
    about: 'markdown in your terminal'
  },
  {
    id: 'next-purgecss',
    date: 'April 2018',
    title: <InlineCode>next-purgecss</InlineCode>,
    url: 'https://github.com/lucleray/next-purgecss',
    about: 'next.js + purgecss for faster websites'
  },
  {
    id: 'next-progressbar',
    date: 'April 2018',
    title: <InlineCode>next-progressbar</InlineCode>,
    url: 'https://github.com/lucleray/next-progressbar',
    about: 'add a progress bar to next.js'
  },
  {
    id: 'hyper-opacity',
    date: 'March 2018',
    title: <InlineCode>hyper-opacity</InlineCode>,
    url: 'https://hyper.is/plugins/hyper-opacity',
    about: 'set the opacity of your hyper terminal'
  },
  {
    id: 'personal-website',
    date: 'December 2017',
    title: <InlineCode>lucleray.me</InlineCode>,
    url: 'https://github.com/lucleray/lucleray.me',
    about: 'my personal website'
  },
  {
    id: 'perceptron',
    date: 'May 2013',
    title: <InlineCode>perceptron</InlineCode>,
    url: 'https://github.com/lucleray/perceptron',
    about: 'an introduction to perceptrons with a demo'
  },
  {
    id: 'ocr-neural-network',
    date: 'Jan to April 2013',
    title: 'OCR with Artificial Neural Networks (in french)',
    url: 'https://drive.google.com/file/d/0B9XryYiAipZ_a0w0b25BYkE4cTg/view'
  },
  {
    id: 'electric-guitar',
    date: '2010',
    title: 'Understanding how an electric guitar work (in french)',
    url: 'http://sculsnay.free.fr/tpe/index.html'
  }
]
