[Local Setup](#local-setup)<br />
[Deploy](#deploy)<br />
[More](#more)

### Local setup

1.  Clone the repo

2.  Install the dependencies

```
yarn install
```

3.  Run locally

```
yarn dev
```

### Deploy

The `master` branch will be automatically deployed when you're pushing commits there.

### More

This project is using :

- next.js (framework)
- styled-components (css-in-js)
